#!/usr/bin/perl
print "\n";
print "\n";
print "Robot Installer Packages are available for the following operating systems \n";
print "-------------------------------------------------------------------------- \n";
print " 1. AIX32 \n";
print " 2. AIX64 \n";
print " 3. LINUX32 \n";
print " 4. LINUX64 \n";
print " 5. zLINUX-x64 \n";
print " 6. LINUX-Ubuntu-amd32 \n";
print " 7. LINUX-Ubuntu-amd64 \n";
print " 8. HPUX-PARISC64 \n";
print " 9. HPUX-IA64 \n";
print " 10. SOLARIS64 \n";
print " 11. SOLARIS-amd64 \n";
print " 12. SOLARIS-i386 \n";
print " 13. WINDOWS32 \n";
print " 14. WINDOWS64 \n";
print " 15. ALL UNIX \n";
print " 16. ALL WINDOWS \n";
print " 17. ALL UNIX & WINDOWS \n";
print "\n";
print " Type the numbers for the Operating system to build package, or type q to quit selection. \n";
print " If you enter more than one number, separate the numbers by a comma. \n";

print "\nYour choice --> ";
chomp($input = <STDIN>);

while ($input >= 18)
{
print "invalid choice re-enter OS Number\n";
print " Type the numbers for the Operating system to build package, or type q to quit selection. \n";
print " If you enter more than one number, separate the numbers by a comma. \n";print "\nYour choice --> ";
chomp($input = <STDIN>);
}


if ($input eq "q") {
  exit;
}
open (FILE,'>',"os_selection.lst") or  die "can't create os_selection.lst file: $!";
foreach $val(sort split /,/, $input) {
print FILE "$val\n";
}
close FILE;
