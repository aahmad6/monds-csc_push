#! /usr/bin/env sh
_extract_dir="/tmp"
_install_dir="/opt"
_install_link_dir="/opt/soe/local"
_robotvarscfg="nms-robot-vars.cfg"
_df="df -k"
_workspace_size="102400"
_service="nimbus"

##Get the uid value
_uid="`id | sed 's/(.*$//' | sed 's/uid=//'`"


## Check for previous installation on Nimsoft robot
check_nimbus_process() {
        if (( $(ps -ef | grep -v grep | grep $_service | wc -l) > 0 ))
        then
        echo "Nimsoft Robot is already installed and running on this machine."
        fi
}

## Check for previous installation on Nimsoft robot
check_nimbus_Install() {
        if [ -f ${_install_dir}/nimsoft/bin/nimbus ]; then
        echo "A previous installation was detected in ${_install_dir}/nimsoft folder. Please uninstall it before running installation again."
        exit 1
        fi
        if [ -f ${_install_link_dir}/nimsoft-1.0/bin/nimbus ]; then
        echo "A previous installation was detected in ${_install_link_dir}/nimsoft-1.0 folder. Please uninstall it before running installation again."
        exit 1
        fi
}

## Check status of Installation
check_niminstall () {
        rpm -qa | grep nimsoft > /dev/null 2>&1
        _status=$?
        if [ $_status -eq 0 ];
        then
        echo "RPM installer found a Nimsoft Robot package already Installed."
        exit 1
        fi
}

## Check for root user - generally need to be root to install Nimsoft robot
check_root()  {
if [ "${_uid}" -ne 0 ]; then
   echo ""
   echo "ERROR: must be root to run ${_script}."
   exit 2
fi
}


## Get the package name to extract
## Package name and installer script must be on the same version
get_pkgname() {
   _pkg=`basename $0`
   _pkgname="${_pkg%.*}"
}


## Return the size in (Kb) of a FS for a given dir
get_size() {
   _dir=$1
   if [ ! -d $_dir ] ; then
      echo "Directory $_dir does not exist"
      return 1
   fi
   _fs_info=`$_df $_dir | grep -v '^Filesystem'`
   if [ $? -ne 0 ] ; then
      echo "$_df $DIR failed"
   fi
   _fs_name=`echo $_fs_info | awk '{ print $1 }'`
   _fs_avail=`echo $_fs_info | awk '{ print $4 }'`
}


## Installs Nimsoft Robot
install_robot() {

## check that there is enough room to extract the package
   get_size ${_install_dir}
    if [ $_workspace_size -ge $_fs_avail ]; then
      echo "This installer requires $_workspace_size Kb in $_fs_name to install.\nIt looks like there is only $_fs_avail Kb available, so Installation aborted."
      exit 3
    fi
     cd ${_extract_dir}
     if [ -f ${_extract_dir}/nimsoft/${_pkgname}.tar ]; then
	tar -xf ${_extract_dir}/nimsoft/${_pkgname}.tar > /dev/null 2>&1
	rpm -ivh ${_extract_dir}/nimsoft/nimsoft-robot*.rpm > /dev/null 2>&1
	cp -pr ${_extract_dir}/nimsoft/$_robotvarscfg ${_install_dir}/$_robotvarscfg > /dev/null 2>&1
 	${_install_dir}/nimsoft/install/RobotConfigurer.sh > /dev/null 2>&1
     else
       echo "No installer package found. Exiting without cleaning up."
       exit 4
     fi
}

## Create a Soft link for the directory /opt/soe/local/nimsoft-1.0
create_link_dir() {
  if [ ! -d ${_install_link_dir} ]; then
    umask 022
    mkdir -p ${_install_link_dir} > /dev/null 2>&1
    ln -s -f ${_install_dir}/nimsoft ${_install_link_dir}/nimsoft-1.0 > /dev/null 2>&1
    unalias rm cp mv > /dev/null 2>&1
  elif [ -h ${_install_link_dir}/nimsoft-1.0 ]; then
      echo "Symbolic link already exists"
  elif [ -d ${_install_link_dir}/nimsoft-1.0 ]; then
      echo "Symbolic link not created"
  else
    ln -s -f ${_install_dir}/nimsoft ${_install_link_dir}/nimsoft-1.0 > /dev/null 2>&1
  fi
}


## Create request.cfg for Auto deploying basic probes
auto_deploy() {
  if [ -d ${_install_dir}/nimsoft ]; then
	if [ -f ${_extract_dir}/nimsoft/request.cfg ]; then
	cp -pr ${_extract_dir}/nimsoft/request.cfg ${_install_dir}/nimsoft/request.cfg > /dev/null 2>&1
	fi
  fi
}


## Cleans up tmp files and dirs
cleanup() {
  if [ -d ${_extract_dir}/nimsoft ]; then
     rm -f ${_extract_dir}/nimsoft/*.rpm > /dev/null 2>&1
     rm -f ${_extract_dir}/nimsoft/*.cfg > /dev/null 2>&1
  fi
}


## Check status of Nimsoft Installation
check_nimstatus () {
        /etc/init.d/nimbus start > /dev/null 2>&1
        sleep 2
        ps -ef | grep nimbus | grep -v grep > /dev/null 2>&1
        _nimstatus=$?
        # echo $_nimstatus
        if [ $_nimstatus -eq 0 ];
        then
        echo "Nimsoft Robot Installed successfully"
	else
	echo "Nimsoft Robot installer failed with Return code $_status. Exiting without cleaning up."
        fi
}


## Main Functions
check_nimbus_process
check_nimbus_Install
check_niminstall
check_root
get_pkgname
install_robot
auto_deploy
check_nimstatus
create_link_dir
cleanup

# exit normally ?
exit $_nimstatus
