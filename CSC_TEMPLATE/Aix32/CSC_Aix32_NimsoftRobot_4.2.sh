#! /usr/bin/env sh
_extract_dir="/tmp/nimsoft"
_install_dir="/opt"
_install_link_dir="/opt/soe/local"
_script=`basename $0`
_df="df -kP"
_workspace_size="102400"
_service="nimbus"

##Get the uid value
_uid="`id | sed 's/(.*$//' | sed 's/uid=//'`"


## Check for previous installation on Nimsoft robot
check_nimbus_process() {
	if (( $(ps -ef | grep -v grep | grep $_service | wc -l) > 0 ))
	then
	echo "Nimsoft Robot is already installed and running on this machine."
	fi
}

## Check for previous installation on Nimsoft robot
check_nimbus_Install() {
	if [ -f ${_install_dir}/nimsoft/bin/nimbus ]; then
        echo "A previous installation was detected in ${_install_dir}/nimsoft folder. Please uninstall it before running installation again."
   	exit 1  
	fi
	if [ -f ${_install_link_dir}/nimsoft-1.0/bin/nimbus ]; then
        echo "A previous installation was detected in ${_install_link_dir}/nimsoft-1.0 folder. Please uninstall it before running installation again."
   	exit 1  
	fi
}


## Check for root user - generally need to be root to install Nimsoft robot
check_root()  {
if [ "${_uid}" -ne 0 ]; then
   echo ""
   echo "ERROR: must be root to run ${_script}."
   exit 2
fi
}


## Get the package name to extract
## Package name and installer script must be on the same version
get_pkgname() {
   _pkg=`basename $0`
   _pkgname="${_pkg%.*}"
}


## Return the size in (Kb) of a FS for a given dir
get_size() {
   _dir=$1
   if [ ! -d $_dir ] ; then
      echo "Directory $_dir does not exist"
      return 1
   fi
   _fs_info=`$_df $_dir | grep -v '^Filesystem'`
   if [ $? -ne 0 ] ; then
      echo "$_df $DIR failed"
   fi
   _fs_name=`echo $_fs_info | awk '{ print $1 }'`
   _fs_avail=`echo $_fs_info | awk '{ print $4 }'`
}


## Create a Soft link for the directory /opt/soe/local/nimsoft-1.0
create_link_dir() {
  if [ ! -d ${_install_link_dir} ]; then
    mkdir -p ${_install_link_dir} > /dev/null 2>&1
    ln -s -f ${_install_dir}/nimsoft ${_install_link_dir}/nimsoft-1.0 > /dev/null 2>&1
  elif [ -h ${_install_link_dir}/nimsoft-1.0 ]; then
      echo "Symbolic link already exists"
  elif [ -d ${_install_link_dir}/nimsoft-1.0 ]; then
      echo "Symbolic link not created"
  else
    ln -s -f ${_install_dir}/nimsoft ${_install_link_dir}/nimsoft-1.0 > /dev/null 2>&1
  fi
}


## Installs Nimsoft Robot
install_robot() {
  if [ -d ${_install_dir} ]; then

## check that there is enough room to extract the package
   get_size ${_install_dir}
    if [ $_workspace_size -ge $_fs_avail ]; then
      echo "This installer requires $_workspace_size Kb in $_fs_name to install.\nIt looks like there is only $_fs_avail Kb available, so Installation aborted."
      exit 3
    fi
     cd ${_install_dir}
     if [ -f ${_extract_dir}/${_pkgname}.tar ]; then
       tar -xf ${_extract_dir}/${_pkgname}.tar 2>/dev/null
       ${_install_dir}/nimsoft/install/RobotConfigurer.sh
       _status=$?
     else
       echo "No installer package found. Exiting without cleaning up."
       exit 4
     fi
 fi
}


## Create request.cfg for Auto deploying basic probes
auto_deploy() {
  if [ -d ${_install_dir}/nimsoft ]; then
        if [ -f ${_extract_dir}/request.cfg ]; then
        cp -pr ${_extract_dir}/request.cfg ${_install_dir}/nimsoft/request.cfg > /dev/null 2>&1
        fi
  fi
}


## Cleans up tmp files and dirs
cleanup() {
  if [ -d ${_extract_dir} ]; then
    rm -f ${_extract_dir}/*-Robot-*.*
  fi
}


## Check status of Installation
check_status () {
  if [ $_status -eq 0 ]; then
      echo "Nimsoft Robot Installed successfully."
      cleanup
      exit 0
  else
     echo "Nimsoft Robot installer failed with Return code $_status. Exiting without cleaning up. "
     exit $_status
  fi
}

## Main Functions
check_nimbus_process
check_nimbus_Install
check_root
get_pkgname
install_robot
create_link_dir
check_status

# exit normally ?
exit $_status
